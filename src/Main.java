import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

import autosar430.autosartoplevelstructure.AUTOSAR;
import autosar430.genericstructure.generaltemplateclasses.admindata.AdminData;


public class Main {
		public static void testDeserialization(String fileuri)
	{
		autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage.eINSTANCE.eClass();
		autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage.eINSTANCE.eClass();
		 // Register the XMI resource factory for the .website extension

        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("arxml", new XMLResourceFactoryImpl());

        // Obtain a new resource set
        StandaloneResourceSet resSet = new StandaloneResourceSet();
        resSet.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA,Boolean.TRUE);
        resSet.getLoadOptions().put(XMLResource.OPTION_DOM_USE_NAMESPACES_IN_SCOPE,Boolean.TRUE);
        // Get the resource
        Resource resource = resSet.getResource(URI
                        .createURI(fileuri), true);
        
        // Get the first model element and cast it to the right type, in my
        // example everything is hierarchical included in this first node
        AUTOSAR myWeb = (AUTOSAR) resource.getContents().get(0);
        System.out.print(myWeb.getAdminData().getShortName());
        
	}
	
	public static void testSerialization(String fileuri)
	{

		AUTOSAR e = autosar430.autosartoplevelstructure.AutosartoplevelstructureFactory.eINSTANCE.createAUTOSAR();
		AdminData ad = autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataFactory.eINSTANCE.createAdminData();
		ad.setShortName("asdasd");
		e.setAdminData(ad);
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("arxml", new XMLResourceFactoryImpl());

     // create a resource
        Resource resource =new XMLResourceImpl(URI.createURI(fileuri));
        
        resource.getContents().add(e);
     // now save the content.
        try {
        		Map<String, Object> options = new HashMap<String, Object>();
        		options.put("ENCODING", "UTF-8");
        		
        		options.put(XMLResource.OPTION_EXTENDED_META_DATA,Boolean.TRUE);
        		options.put(XMIResource.OPTION_SCHEMA_LOCATION,Boolean.TRUE);
        	
                resource.save(options);
        } catch (IOException ee) {
                // TODO Auto-generated catch block
                ee.printStackTrace();
        }
	}

	public static void main(String[] args) {
		// Testing serialization of model object to an autosarfile
		testSerialization("tests/test1.arxml");
		
		//Testing deserialization of an autosar model object
		testDeserialization("tests/test1.arxml");
	}

}

/**
 */
package autosar430.genericstructure.generaltemplateclasses.identifiable;

import autosar430.genericstructure.generaltemplateclasses.arobject.ARObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see autosar430.genericstructure.generaltemplateclasses.identifiable.IdentifiablePackage#getIdentifiable()
 * @model abstract="true"
 * @generated
 */
public interface Identifiable extends ARObject {
} // Identifiable

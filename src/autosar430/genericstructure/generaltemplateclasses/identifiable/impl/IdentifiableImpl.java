/**
 */
package autosar430.genericstructure.generaltemplateclasses.identifiable.impl;

import autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl;

import autosar430.genericstructure.generaltemplateclasses.identifiable.Identifiable;
import autosar430.genericstructure.generaltemplateclasses.identifiable.IdentifiablePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class IdentifiableImpl extends ARObjectImpl implements Identifiable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdentifiableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return IdentifiablePackage.Literals.IDENTIFIABLE;
	}

} //IdentifiableImpl

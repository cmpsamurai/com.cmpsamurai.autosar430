/**
 */
package autosar430.genericstructure.generaltemplateclasses.arobject.impl;

import autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage;

import autosar430.autosartoplevelstructure.impl.AutosartoplevelstructurePackageImpl;

import autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage;
import autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataPackageImpl;
import autosar430.genericstructure.generaltemplateclasses.arobject.ARObject;
import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectFactory;
import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage;

import autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage;
import autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl;
import autosar430.genericstructure.generaltemplateclasses.identifiable.IdentifiablePackage;
import autosar430.genericstructure.generaltemplateclasses.identifiable.impl.IdentifiablePackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArobjectPackageImpl extends EPackageImpl implements ArobjectPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arObjectEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ArobjectPackageImpl() {
		super(eNS_URI, ArobjectFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ArobjectPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ArobjectPackage init() {
		if (isInited) return (ArobjectPackage)EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI);

		// Obtain or create and register package
		ArobjectPackageImpl theArobjectPackage = (ArobjectPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ArobjectPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ArobjectPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AutosartoplevelstructurePackageImpl theAutosartoplevelstructurePackage = (AutosartoplevelstructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AutosartoplevelstructurePackage.eNS_URI) instanceof AutosartoplevelstructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AutosartoplevelstructurePackage.eNS_URI) : AutosartoplevelstructurePackage.eINSTANCE);
		AdmindataPackageImpl theAdmindataPackage = (AdmindataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI) instanceof AdmindataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI) : AdmindataPackage.eINSTANCE);
		ArpackagePackageImpl theArpackagePackage = (ArpackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ArpackagePackage.eNS_URI) instanceof ArpackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ArpackagePackage.eNS_URI) : ArpackagePackage.eINSTANCE);
		IdentifiablePackageImpl theIdentifiablePackage = (IdentifiablePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdentifiablePackage.eNS_URI) instanceof IdentifiablePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdentifiablePackage.eNS_URI) : IdentifiablePackage.eINSTANCE);

		// Create package meta-data objects
		theArobjectPackage.createPackageContents();
		theAutosartoplevelstructurePackage.createPackageContents();
		theAdmindataPackage.createPackageContents();
		theArpackagePackage.createPackageContents();
		theIdentifiablePackage.createPackageContents();

		// Initialize created meta-data
		theArobjectPackage.initializePackageContents();
		theAutosartoplevelstructurePackage.initializePackageContents();
		theAdmindataPackage.initializePackageContents();
		theArpackagePackage.initializePackageContents();
		theIdentifiablePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theArobjectPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ArobjectPackage.eNS_URI, theArobjectPackage);
		return theArobjectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getARObject() {
		return arObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getARObject_Checksum() {
		return (EAttribute)arObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getARObject_Timestamp() {
		return (EAttribute)arObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArobjectFactory getArobjectFactory() {
		return (ArobjectFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		arObjectEClass = createEClass(AR_OBJECT);
		createEAttribute(arObjectEClass, AR_OBJECT__CHECKSUM);
		createEAttribute(arObjectEClass, AR_OBJECT__TIMESTAMP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(arObjectEClass, ARObject.class, "ARObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getARObject_Checksum(), ecorePackage.getEString(), "checksum", null, 0, 1, ARObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getARObject_Timestamp(), ecorePackage.getEDate(), "timestamp", null, 0, 1, ARObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ArobjectPackageImpl

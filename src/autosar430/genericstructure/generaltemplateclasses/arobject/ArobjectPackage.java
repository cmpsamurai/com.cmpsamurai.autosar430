/**
 */
package autosar430.genericstructure.generaltemplateclasses.arobject;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectFactory
 * @model kind="package"
 * @generated
 */
public interface ArobjectPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "arobject";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://autosar.org/schema/r4.0/genericstructure/generaltemplateclasses/arobject";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "arobject";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArobjectPackage eINSTANCE = autosar430.genericstructure.generaltemplateclasses.arobject.impl.ArobjectPackageImpl.init();

	/**
	 * The meta object id for the '{@link autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl <em>AR Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl
	 * @see autosar430.genericstructure.generaltemplateclasses.arobject.impl.ArobjectPackageImpl#getARObject()
	 * @generated
	 */
	int AR_OBJECT = 0;

	/**
	 * The feature id for the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_OBJECT__CHECKSUM = 0;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_OBJECT__TIMESTAMP = 1;

	/**
	 * The number of structural features of the '<em>AR Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_OBJECT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>AR Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_OBJECT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link autosar430.genericstructure.generaltemplateclasses.arobject.ARObject <em>AR Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AR Object</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arobject.ARObject
	 * @generated
	 */
	EClass getARObject();

	/**
	 * Returns the meta object for the attribute '{@link autosar430.genericstructure.generaltemplateclasses.arobject.ARObject#getChecksum <em>Checksum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checksum</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arobject.ARObject#getChecksum()
	 * @see #getARObject()
	 * @generated
	 */
	EAttribute getARObject_Checksum();

	/**
	 * Returns the meta object for the attribute '{@link autosar430.genericstructure.generaltemplateclasses.arobject.ARObject#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arobject.ARObject#getTimestamp()
	 * @see #getARObject()
	 * @generated
	 */
	EAttribute getARObject_Timestamp();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ArobjectFactory getArobjectFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl <em>AR Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl
		 * @see autosar430.genericstructure.generaltemplateclasses.arobject.impl.ArobjectPackageImpl#getARObject()
		 * @generated
		 */
		EClass AR_OBJECT = eINSTANCE.getARObject();

		/**
		 * The meta object literal for the '<em><b>Checksum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AR_OBJECT__CHECKSUM = eINSTANCE.getARObject_Checksum();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AR_OBJECT__TIMESTAMP = eINSTANCE.getARObject_Timestamp();

	}

} //ArobjectPackage

/**
 */
package autosar430.genericstructure.generaltemplateclasses.arobject;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage
 * @generated
 */
public interface ArobjectFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArobjectFactory eINSTANCE = autosar430.genericstructure.generaltemplateclasses.arobject.impl.ArobjectFactoryImpl.init();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ArobjectPackage getArobjectPackage();

} //ArobjectFactory

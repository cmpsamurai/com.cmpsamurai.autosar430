/**
 */
package autosar430.genericstructure.generaltemplateclasses.admindata;

import autosar430.genericstructure.generaltemplateclasses.arobject.ARObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Admin Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link autosar430.genericstructure.generaltemplateclasses.admindata.AdminData#getShortName <em>Short Name</em>}</li>
 * </ul>
 *
 * @see autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage#getAdminData()
 * @model extendedMetaData="name='ADMIN-DATA' kind='elementOnly'"
 * @generated
 */
public interface AdminData extends ARObject {

	/**
	 * Returns the value of the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Name</em>' attribute.
	 * @see #setShortName(String)
	 * @see autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage#getAdminData_ShortName()
	 * @model
	 * @generated
	 */
	String getShortName();

	/**
	 * Sets the value of the '{@link autosar430.genericstructure.generaltemplateclasses.admindata.AdminData#getShortName <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Name</em>' attribute.
	 * @see #getShortName()
	 * @generated
	 */
	void setShortName(String value);
} // AdminData

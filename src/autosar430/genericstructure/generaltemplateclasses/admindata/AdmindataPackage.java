/**
 */
package autosar430.genericstructure.generaltemplateclasses.admindata;

import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataFactory
 * @model kind="package"
 * @generated
 */
public interface AdmindataPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "admindata";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://autosar.org/schema/r4.0/genericstructure/generaltemplateclasses/admindata";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "admindata";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdmindataPackage eINSTANCE = autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataPackageImpl.init();

	/**
	 * The meta object id for the '{@link autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdminDataImpl <em>Admin Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdminDataImpl
	 * @see autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataPackageImpl#getAdminData()
	 * @generated
	 */
	int ADMIN_DATA = 0;

	/**
	 * The feature id for the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_DATA__CHECKSUM = ArobjectPackage.AR_OBJECT__CHECKSUM;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_DATA__TIMESTAMP = ArobjectPackage.AR_OBJECT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_DATA__SHORT_NAME = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Admin Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_DATA_FEATURE_COUNT = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Admin Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_DATA_OPERATION_COUNT = ArobjectPackage.AR_OBJECT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link autosar430.genericstructure.generaltemplateclasses.admindata.AdminData <em>Admin Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Admin Data</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.admindata.AdminData
	 * @generated
	 */
	EClass getAdminData();

	/**
	 * Returns the meta object for the attribute '{@link autosar430.genericstructure.generaltemplateclasses.admindata.AdminData#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.admindata.AdminData#getShortName()
	 * @see #getAdminData()
	 * @generated
	 */
	EAttribute getAdminData_ShortName();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AdmindataFactory getAdmindataFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdminDataImpl <em>Admin Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdminDataImpl
		 * @see autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataPackageImpl#getAdminData()
		 * @generated
		 */
		EClass ADMIN_DATA = eINSTANCE.getAdminData();
		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADMIN_DATA__SHORT_NAME = eINSTANCE.getAdminData_ShortName();

	}

} //AdmindataPackage

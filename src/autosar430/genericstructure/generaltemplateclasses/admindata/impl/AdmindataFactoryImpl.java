/**
 */
package autosar430.genericstructure.generaltemplateclasses.admindata.impl;

import autosar430.genericstructure.generaltemplateclasses.admindata.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdmindataFactoryImpl extends EFactoryImpl implements AdmindataFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AdmindataFactory init() {
		try {
			AdmindataFactory theAdmindataFactory = (AdmindataFactory)EPackage.Registry.INSTANCE.getEFactory(AdmindataPackage.eNS_URI);
			if (theAdmindataFactory != null) {
				return theAdmindataFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AdmindataFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdmindataFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AdmindataPackage.ADMIN_DATA: return createAdminData();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdminData createAdminData() {
		AdminDataImpl adminData = new AdminDataImpl();
		return adminData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdmindataPackage getAdmindataPackage() {
		return (AdmindataPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AdmindataPackage getPackage() {
		return AdmindataPackage.eINSTANCE;
	}

} //AdmindataFactoryImpl

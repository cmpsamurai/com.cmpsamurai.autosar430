/**
 */
package autosar430.genericstructure.generaltemplateclasses.admindata;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage
 * @generated
 */
public interface AdmindataFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdmindataFactory eINSTANCE = autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Admin Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Admin Data</em>'.
	 * @generated
	 */
	AdminData createAdminData();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AdmindataPackage getAdmindataPackage();

} //AdmindataFactory

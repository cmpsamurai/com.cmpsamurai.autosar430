/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage;

import autosar430.genericstructure.generaltemplateclasses.arobject.ARObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AR Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage#getARElement()
 * @model abstract="true"
 * @generated
 */
public interface ARElement extends ARObject {
} // ARElement

/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Packageable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage#getPackageableElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface PackageableElement extends EObject {
} // PackageableElement

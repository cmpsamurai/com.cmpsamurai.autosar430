/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage;

import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackageFactory
 * @model kind="package"
 * @generated
 */
public interface ArpackagePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "arpackage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://autosar.org/schema/r4.0/genericstructure/generaltemplateclasses/arpackage";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "arpackage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArpackagePackage eINSTANCE = autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl.init();

	/**
	 * The meta object id for the '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARElementImpl <em>AR Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARElementImpl
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl#getARElement()
	 * @generated
	 */
	int AR_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_ELEMENT__CHECKSUM = ArobjectPackage.AR_OBJECT__CHECKSUM;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_ELEMENT__TIMESTAMP = ArobjectPackage.AR_OBJECT__TIMESTAMP;

	/**
	 * The number of structural features of the '<em>AR Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_ELEMENT_FEATURE_COUNT = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>AR Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_ELEMENT_OPERATION_COUNT = ArobjectPackage.AR_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARPackageImpl <em>AR Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARPackageImpl
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl#getARPackage()
	 * @generated
	 */
	int AR_PACKAGE = 1;

	/**
	 * The feature id for the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_PACKAGE__CHECKSUM = ArobjectPackage.AR_OBJECT__CHECKSUM;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_PACKAGE__TIMESTAMP = ArobjectPackage.AR_OBJECT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Ar Package</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_PACKAGE__AR_PACKAGE = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_PACKAGE__ELEMENT = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AR Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_PACKAGE_FEATURE_COUNT = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>AR Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AR_PACKAGE_OPERATION_COUNT = ArobjectPackage.AR_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement <em>Packageable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl#getPackageableElement()
	 * @generated
	 */
	int PACKAGEABLE_ELEMENT = 2;

	/**
	 * The number of structural features of the '<em>Packageable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGEABLE_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Packageable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGEABLE_ELEMENT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARElement <em>AR Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AR Element</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ARElement
	 * @generated
	 */
	EClass getARElement();

	/**
	 * Returns the meta object for class '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage <em>AR Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AR Package</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage
	 * @generated
	 */
	EClass getARPackage();

	/**
	 * Returns the meta object for the reference list '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage#getArPackage <em>Ar Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ar Package</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage#getArPackage()
	 * @see #getARPackage()
	 * @generated
	 */
	EReference getARPackage_ArPackage();

	/**
	 * Returns the meta object for the reference list '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Element</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage#getElement()
	 * @see #getARPackage()
	 * @generated
	 */
	EReference getARPackage_Element();

	/**
	 * Returns the meta object for class '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement <em>Packageable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Packageable Element</em>'.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement
	 * @generated
	 */
	EClass getPackageableElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ArpackageFactory getArpackageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARElementImpl <em>AR Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARElementImpl
		 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl#getARElement()
		 * @generated
		 */
		EClass AR_ELEMENT = eINSTANCE.getARElement();

		/**
		 * The meta object literal for the '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARPackageImpl <em>AR Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARPackageImpl
		 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl#getARPackage()
		 * @generated
		 */
		EClass AR_PACKAGE = eINSTANCE.getARPackage();

		/**
		 * The meta object literal for the '<em><b>Ar Package</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AR_PACKAGE__AR_PACKAGE = eINSTANCE.getARPackage_ArPackage();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AR_PACKAGE__ELEMENT = eINSTANCE.getARPackage_Element();

		/**
		 * The meta object literal for the '{@link autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement <em>Packageable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement
		 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl#getPackageableElement()
		 * @generated
		 */
		EClass PACKAGEABLE_ELEMENT = eINSTANCE.getPackageableElement();

	}

} //ArpackagePackage

/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage;

import autosar430.genericstructure.generaltemplateclasses.arobject.ARObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AR Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage#getArPackage <em>Ar Package</em>}</li>
 *   <li>{@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage#getElement <em>Element</em>}</li>
 * </ul>
 *
 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage#getARPackage()
 * @model
 * @generated
 */
public interface ARPackage extends ARObject {
	/**
	 * Returns the value of the '<em><b>Ar Package</b></em>' reference list.
	 * The list contents are of type {@link autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ar Package</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ar Package</em>' reference list.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage#getARPackage_ArPackage()
	 * @model
	 * @generated
	 */
	EList<ARPackage> getArPackage();

	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference list.
	 * The list contents are of type {@link autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' reference list.
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage#getARPackage_Element()
	 * @model
	 * @generated
	 */
	EList<PackageableElement> getElement();

} // ARPackage

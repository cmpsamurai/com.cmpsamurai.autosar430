/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage
 * @generated
 */
public interface ArpackageFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArpackageFactory eINSTANCE = autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>AR Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AR Package</em>'.
	 * @generated
	 */
	ARPackage createARPackage();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ArpackagePackage getArpackagePackage();

} //ArpackageFactory

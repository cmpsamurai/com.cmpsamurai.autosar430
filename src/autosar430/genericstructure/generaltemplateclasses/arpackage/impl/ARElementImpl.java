/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage.impl;

import autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl;

import autosar430.genericstructure.generaltemplateclasses.arpackage.ARElement;
import autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AR Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ARElementImpl extends ARObjectImpl implements ARElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ARElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArpackagePackage.Literals.AR_ELEMENT;
	}

} //ARElementImpl

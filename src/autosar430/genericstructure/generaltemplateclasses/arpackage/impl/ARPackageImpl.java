/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage.impl;

import autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl;

import autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage;
import autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage;
import autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AR Package</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARPackageImpl#getArPackage <em>Ar Package</em>}</li>
 *   <li>{@link autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ARPackageImpl#getElement <em>Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ARPackageImpl extends ARObjectImpl implements ARPackage {
	/**
	 * The cached value of the '{@link #getArPackage() <em>Ar Package</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArPackage()
	 * @generated
	 * @ordered
	 */
	protected EList<ARPackage> arPackage;

	/**
	 * The cached value of the '{@link #getElement() <em>Element</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement()
	 * @generated
	 * @ordered
	 */
	protected EList<PackageableElement> element;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ARPackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArpackagePackage.Literals.AR_PACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ARPackage> getArPackage() {
		if (arPackage == null) {
			arPackage = new EObjectResolvingEList<ARPackage>(ARPackage.class, this, ArpackagePackage.AR_PACKAGE__AR_PACKAGE);
		}
		return arPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageableElement> getElement() {
		if (element == null) {
			element = new EObjectResolvingEList<PackageableElement>(PackageableElement.class, this, ArpackagePackage.AR_PACKAGE__ELEMENT);
		}
		return element;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ArpackagePackage.AR_PACKAGE__AR_PACKAGE:
				return getArPackage();
			case ArpackagePackage.AR_PACKAGE__ELEMENT:
				return getElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ArpackagePackage.AR_PACKAGE__AR_PACKAGE:
				getArPackage().clear();
				getArPackage().addAll((Collection<? extends ARPackage>)newValue);
				return;
			case ArpackagePackage.AR_PACKAGE__ELEMENT:
				getElement().clear();
				getElement().addAll((Collection<? extends PackageableElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ArpackagePackage.AR_PACKAGE__AR_PACKAGE:
				getArPackage().clear();
				return;
			case ArpackagePackage.AR_PACKAGE__ELEMENT:
				getElement().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ArpackagePackage.AR_PACKAGE__AR_PACKAGE:
				return arPackage != null && !arPackage.isEmpty();
			case ArpackagePackage.AR_PACKAGE__ELEMENT:
				return element != null && !element.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ARPackageImpl

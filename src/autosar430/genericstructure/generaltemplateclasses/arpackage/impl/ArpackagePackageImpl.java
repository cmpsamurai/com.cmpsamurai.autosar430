/**
 */
package autosar430.genericstructure.generaltemplateclasses.arpackage.impl;

import autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage;

import autosar430.autosartoplevelstructure.impl.AutosartoplevelstructurePackageImpl;

import autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage;

import autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataPackageImpl;

import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage;

import autosar430.genericstructure.generaltemplateclasses.arobject.impl.ArobjectPackageImpl;

import autosar430.genericstructure.generaltemplateclasses.arpackage.ARElement;
import autosar430.genericstructure.generaltemplateclasses.arpackage.ARPackage;
import autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackageFactory;
import autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage;
import autosar430.genericstructure.generaltemplateclasses.arpackage.PackageableElement;

import autosar430.genericstructure.generaltemplateclasses.identifiable.IdentifiablePackage;

import autosar430.genericstructure.generaltemplateclasses.identifiable.impl.IdentifiablePackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ArpackagePackageImpl extends EPackageImpl implements ArpackagePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass arPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packageableElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ArpackagePackageImpl() {
		super(eNS_URI, ArpackageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ArpackagePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ArpackagePackage init() {
		if (isInited) return (ArpackagePackage)EPackage.Registry.INSTANCE.getEPackage(ArpackagePackage.eNS_URI);

		// Obtain or create and register package
		ArpackagePackageImpl theArpackagePackage = (ArpackagePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ArpackagePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ArpackagePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AutosartoplevelstructurePackageImpl theAutosartoplevelstructurePackage = (AutosartoplevelstructurePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AutosartoplevelstructurePackage.eNS_URI) instanceof AutosartoplevelstructurePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AutosartoplevelstructurePackage.eNS_URI) : AutosartoplevelstructurePackage.eINSTANCE);
		ArobjectPackageImpl theArobjectPackage = (ArobjectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI) instanceof ArobjectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI) : ArobjectPackage.eINSTANCE);
		AdmindataPackageImpl theAdmindataPackage = (AdmindataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI) instanceof AdmindataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI) : AdmindataPackage.eINSTANCE);
		IdentifiablePackageImpl theIdentifiablePackage = (IdentifiablePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdentifiablePackage.eNS_URI) instanceof IdentifiablePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdentifiablePackage.eNS_URI) : IdentifiablePackage.eINSTANCE);

		// Create package meta-data objects
		theArpackagePackage.createPackageContents();
		theAutosartoplevelstructurePackage.createPackageContents();
		theArobjectPackage.createPackageContents();
		theAdmindataPackage.createPackageContents();
		theIdentifiablePackage.createPackageContents();

		// Initialize created meta-data
		theArpackagePackage.initializePackageContents();
		theAutosartoplevelstructurePackage.initializePackageContents();
		theArobjectPackage.initializePackageContents();
		theAdmindataPackage.initializePackageContents();
		theIdentifiablePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theArpackagePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ArpackagePackage.eNS_URI, theArpackagePackage);
		return theArpackagePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getARElement() {
		return arElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getARPackage() {
		return arPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getARPackage_ArPackage() {
		return (EReference)arPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getARPackage_Element() {
		return (EReference)arPackageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPackageableElement() {
		return packageableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArpackageFactory getArpackageFactory() {
		return (ArpackageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		arElementEClass = createEClass(AR_ELEMENT);

		arPackageEClass = createEClass(AR_PACKAGE);
		createEReference(arPackageEClass, AR_PACKAGE__AR_PACKAGE);
		createEReference(arPackageEClass, AR_PACKAGE__ELEMENT);

		packageableElementEClass = createEClass(PACKAGEABLE_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ArobjectPackage theArobjectPackage = (ArobjectPackage)EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		arElementEClass.getESuperTypes().add(theArobjectPackage.getARObject());
		arPackageEClass.getESuperTypes().add(theArobjectPackage.getARObject());

		// Initialize classes, features, and operations; add parameters
		initEClass(arElementEClass, ARElement.class, "ARElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(arPackageEClass, ARPackage.class, "ARPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getARPackage_ArPackage(), this.getARPackage(), null, "arPackage", null, 0, -1, ARPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getARPackage_Element(), this.getPackageableElement(), null, "element", null, 0, -1, ARPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(packageableElementEClass, PackageableElement.class, "PackageableElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ArpackagePackageImpl

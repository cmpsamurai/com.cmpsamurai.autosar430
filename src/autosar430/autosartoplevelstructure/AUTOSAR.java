/**
 */
package autosar430.autosartoplevelstructure;

import autosar430.genericstructure.generaltemplateclasses.admindata.AdminData;
import autosar430.genericstructure.generaltemplateclasses.arobject.ARObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AUTOSAR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link autosar430.autosartoplevelstructure.AUTOSAR#getAdminData <em>Admin Data</em>}</li>
 * </ul>
 *
 * @see autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage#getAUTOSAR()
 * @model
 * @generated
 */
public interface AUTOSAR extends ARObject {

	/**
	 * Returns the value of the '<em><b>Admin Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Admin Data</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Admin Data</em>' containment reference.
	 * @see #setAdminData(AdminData)
	 * @see autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage#getAUTOSAR_AdminData()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	AdminData getAdminData();

	/**
	 * Sets the value of the '{@link autosar430.autosartoplevelstructure.AUTOSAR#getAdminData <em>Admin Data</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Admin Data</em>' containment reference.
	 * @see #getAdminData()
	 * @generated
	 */
	void setAdminData(AdminData value);
} // AUTOSAR

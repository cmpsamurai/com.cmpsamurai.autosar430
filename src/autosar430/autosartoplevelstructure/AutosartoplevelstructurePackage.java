/**
 */
package autosar430.autosartoplevelstructure;

import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see autosar430.autosartoplevelstructure.AutosartoplevelstructureFactory
 * @model kind="package"
 * @generated
 */
public interface AutosartoplevelstructurePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "autosartoplevelstructure";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://autosar.org/schema/r4.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AutosartoplevelstructurePackage eINSTANCE = autosar430.autosartoplevelstructure.impl.AutosartoplevelstructurePackageImpl.init();

	/**
	 * The meta object id for the '{@link autosar430.autosartoplevelstructure.impl.AUTOSARImpl <em>AUTOSAR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see autosar430.autosartoplevelstructure.impl.AUTOSARImpl
	 * @see autosar430.autosartoplevelstructure.impl.AutosartoplevelstructurePackageImpl#getAUTOSAR()
	 * @generated
	 */
	int AUTOSAR = 0;

	/**
	 * The feature id for the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSAR__CHECKSUM = ArobjectPackage.AR_OBJECT__CHECKSUM;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSAR__TIMESTAMP = ArobjectPackage.AR_OBJECT__TIMESTAMP;

	/**
	 * The feature id for the '<em><b>Admin Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSAR__ADMIN_DATA = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AUTOSAR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSAR_FEATURE_COUNT = ArobjectPackage.AR_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AUTOSAR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOSAR_OPERATION_COUNT = ArobjectPackage.AR_OBJECT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link autosar430.autosartoplevelstructure.AUTOSAR <em>AUTOSAR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AUTOSAR</em>'.
	 * @see autosar430.autosartoplevelstructure.AUTOSAR
	 * @generated
	 */
	EClass getAUTOSAR();

	/**
	 * Returns the meta object for the containment reference '{@link autosar430.autosartoplevelstructure.AUTOSAR#getAdminData <em>Admin Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Admin Data</em>'.
	 * @see autosar430.autosartoplevelstructure.AUTOSAR#getAdminData()
	 * @see #getAUTOSAR()
	 * @generated
	 */
	EReference getAUTOSAR_AdminData();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AutosartoplevelstructureFactory getAutosartoplevelstructureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link autosar430.autosartoplevelstructure.impl.AUTOSARImpl <em>AUTOSAR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see autosar430.autosartoplevelstructure.impl.AUTOSARImpl
		 * @see autosar430.autosartoplevelstructure.impl.AutosartoplevelstructurePackageImpl#getAUTOSAR()
		 * @generated
		 */
		EClass AUTOSAR = eINSTANCE.getAUTOSAR();
		/**
		 * The meta object literal for the '<em><b>Admin Data</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTOSAR__ADMIN_DATA = eINSTANCE.getAUTOSAR_AdminData();

	}

} //AutosartoplevelstructurePackage

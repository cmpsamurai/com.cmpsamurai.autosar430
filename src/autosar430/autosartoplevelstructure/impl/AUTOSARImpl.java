/**
 */
package autosar430.autosartoplevelstructure.impl;

import autosar430.autosartoplevelstructure.AUTOSAR;
import autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage;

import autosar430.genericstructure.generaltemplateclasses.admindata.AdminData;
import autosar430.genericstructure.generaltemplateclasses.arobject.impl.ARObjectImpl;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AUTOSAR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link autosar430.autosartoplevelstructure.impl.AUTOSARImpl#getAdminData <em>Admin Data</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AUTOSARImpl extends ARObjectImpl implements AUTOSAR {
	/**
	 * The cached value of the '{@link #getAdminData() <em>Admin Data</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdminData()
	 * @generated
	 * @ordered
	 */
	protected AdminData adminData;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AUTOSARImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AutosartoplevelstructurePackage.Literals.AUTOSAR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdminData getAdminData() {
		return adminData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdminData(AdminData newAdminData, NotificationChain msgs) {
		AdminData oldAdminData = adminData;
		adminData = newAdminData;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA, oldAdminData, newAdminData);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdminData(AdminData newAdminData) {
		if (newAdminData != adminData) {
			NotificationChain msgs = null;
			if (adminData != null)
				msgs = ((InternalEObject)adminData).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA, null, msgs);
			if (newAdminData != null)
				msgs = ((InternalEObject)newAdminData).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA, null, msgs);
			msgs = basicSetAdminData(newAdminData, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA, newAdminData, newAdminData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA:
				return basicSetAdminData(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA:
				return getAdminData();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA:
				setAdminData((AdminData)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA:
				setAdminData((AdminData)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AutosartoplevelstructurePackage.AUTOSAR__ADMIN_DATA:
				return adminData != null;
		}
		return super.eIsSet(featureID);
	}

} //AUTOSARImpl

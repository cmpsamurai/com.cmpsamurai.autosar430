/**
 */
package autosar430.autosartoplevelstructure.impl;

import autosar430.autosartoplevelstructure.AutosartoplevelstructureFactory;
import autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage;

import autosar430.genericstructure.generaltemplateclasses.admindata.AdmindataPackage;
import autosar430.genericstructure.generaltemplateclasses.admindata.impl.AdmindataPackageImpl;
import autosar430.genericstructure.generaltemplateclasses.arobject.ArobjectPackage;

import autosar430.genericstructure.generaltemplateclasses.arobject.impl.ArobjectPackageImpl;

import autosar430.genericstructure.generaltemplateclasses.arpackage.ArpackagePackage;
import autosar430.genericstructure.generaltemplateclasses.arpackage.impl.ArpackagePackageImpl;
import autosar430.genericstructure.generaltemplateclasses.identifiable.IdentifiablePackage;
import autosar430.genericstructure.generaltemplateclasses.identifiable.impl.IdentifiablePackageImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AutosartoplevelstructurePackageImpl extends EPackageImpl implements AutosartoplevelstructurePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass autosarEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see autosar430.autosartoplevelstructure.AutosartoplevelstructurePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AutosartoplevelstructurePackageImpl() {
		super(eNS_URI, AutosartoplevelstructureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AutosartoplevelstructurePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AutosartoplevelstructurePackage init() {
		if (isInited) return (AutosartoplevelstructurePackage)EPackage.Registry.INSTANCE.getEPackage(AutosartoplevelstructurePackage.eNS_URI);

		// Obtain or create and register package
		AutosartoplevelstructurePackageImpl theAutosartoplevelstructurePackage = (AutosartoplevelstructurePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AutosartoplevelstructurePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AutosartoplevelstructurePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		ArobjectPackageImpl theArobjectPackage = (ArobjectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI) instanceof ArobjectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI) : ArobjectPackage.eINSTANCE);
		AdmindataPackageImpl theAdmindataPackage = (AdmindataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI) instanceof AdmindataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI) : AdmindataPackage.eINSTANCE);
		ArpackagePackageImpl theArpackagePackage = (ArpackagePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ArpackagePackage.eNS_URI) instanceof ArpackagePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ArpackagePackage.eNS_URI) : ArpackagePackage.eINSTANCE);
		IdentifiablePackageImpl theIdentifiablePackage = (IdentifiablePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(IdentifiablePackage.eNS_URI) instanceof IdentifiablePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(IdentifiablePackage.eNS_URI) : IdentifiablePackage.eINSTANCE);

		// Create package meta-data objects
		theAutosartoplevelstructurePackage.createPackageContents();
		theArobjectPackage.createPackageContents();
		theAdmindataPackage.createPackageContents();
		theArpackagePackage.createPackageContents();
		theIdentifiablePackage.createPackageContents();

		// Initialize created meta-data
		theAutosartoplevelstructurePackage.initializePackageContents();
		theArobjectPackage.initializePackageContents();
		theAdmindataPackage.initializePackageContents();
		theArpackagePackage.initializePackageContents();
		theIdentifiablePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAutosartoplevelstructurePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AutosartoplevelstructurePackage.eNS_URI, theAutosartoplevelstructurePackage);
		return theAutosartoplevelstructurePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAUTOSAR() {
		return autosarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAUTOSAR_AdminData() {
		return (EReference)autosarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutosartoplevelstructureFactory getAutosartoplevelstructureFactory() {
		return (AutosartoplevelstructureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		autosarEClass = createEClass(AUTOSAR);
		createEReference(autosarEClass, AUTOSAR__ADMIN_DATA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ArobjectPackage theArobjectPackage = (ArobjectPackage)EPackage.Registry.INSTANCE.getEPackage(ArobjectPackage.eNS_URI);
		AdmindataPackage theAdmindataPackage = (AdmindataPackage)EPackage.Registry.INSTANCE.getEPackage(AdmindataPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		autosarEClass.getESuperTypes().add(theArobjectPackage.getARObject());

		// Initialize classes, features, and operations; add parameters
		initEClass(autosarEClass, autosar430.autosartoplevelstructure.AUTOSAR.class, "AUTOSAR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAUTOSAR_AdminData(), theAdmindataPackage.getAdminData(), null, "adminData", null, 0, 1, autosar430.autosartoplevelstructure.AUTOSAR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
		addAnnotation
		  (getAUTOSAR_AdminData(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "ADMIN-DATA",
			 "namespace", "##targetNamespace"
		   });
	}
	
	@Override
	protected Resource createResource(String uri) {
		// TODO Auto-generated method stub
		return super.createResource("AUTOSAR_4-3-0.xsd");
	}

} //AutosartoplevelstructurePackageImpl

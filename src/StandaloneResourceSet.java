import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class StandaloneResourceSet extends ResourceSetImpl {
  
  @Override
  public EObject getEObject(URI uri, boolean loadOnDemand){
    if (uri == null) {
      return null;
    }

    // Target element may be in any of the specified resources
    EObject resolvedEObject = null;
    for (Resource resource : resources) {
      try {
        EObject eObject = resource.getEObject(uri.fragment());
        if (eObject != null) {
          resolvedEObject = eObject;
          break;
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }

    return resolvedEObject;
  }

}
